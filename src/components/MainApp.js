import React, { Component } from 'react';
import Search from './Search';
import Folder from './Folder';
import ImageResults from './ImageResults';
import VideoResults from './VideoResults';
import axios from "axios/index";
require("../styles/MainApp.css");


let folder1 = 'cars';
let folder2 = 'bugs';
let folder3 = 'skyline';
let folder4 = 'art';

class MainApp extends Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }

    // initialize state
    state = {
        searchText: '',
        amount: 50,
        apiUrl: 'https://pixabay.com/api',
        apiKey: '9816835-ef2a6e46c2a9886e7ae775463',
        images: [],
        videos: []
    };


    // when a folder is clicked, search pixabay for pictures that match folder search criteria
    onClick = event => {
        const val = event.target.id;
        this.setState({ videos: [], images: [] });
        this.setState({ searchText: val }, () => {
            if (val === '') {
                this.setState({ images: [] });
            } else {
                axios
                    .get(
                        `${this.state.apiUrl}/?key=${this.state.apiKey}&q=${
                            this.state.searchText
                            }&image_type=all&per_page=50&safesearch=true`
                    )
                    .then(res => this.setState({ images: res.data.hits }))
                    .catch(err => console.log(err));
            }
        });
    };

    // retrieve videos from pixabay when search input changes
    onClickVideos = event => {
        const val = event.target.id;
        this.setState({ videos: [], images: [] });
        this.setState({ searchText: val }, () => {
            if (val === '') {
                this.setState({ videos: [] });
            } else {
                axios
                    .get(
                        `${this.state.apiUrl}/videos/?key=${this.state.apiKey}&q=${
                            this.state.searchText
                            }&video_type=all&per_page=50&safesearch=true`
                    )
                    .then(res => this.setState({ videos: res.data.hits }))
                    .catch(err => console.log(err));
            }
        });
    };


    // retrieve images from pixabay when search input changes
    onTextChange = e => {
        const val = e.target.value;
        this.setState({ videos: [], images: [] });
        this.setState({ searchText: val }, () => {
            if (val === '') {
                this.setState({ images: [] });
            } else {
                axios
                    .get(
                        `${this.state.apiUrl}/?key=${this.state.apiKey}&q=${
                            this.state.searchText
                            }&image_type=all&per_page=50&safesearch=true`
                    )
                    .then(res => this.setState({ images: res.data.hits }))
                    .catch(err => console.log(err));
            }
        });
    };

    render() {
        return (

        <div>
            <form>
                <div className="folder-selector">
                    <Folder searchCriteria={folder1} handler={this.onClick}/>
                    <Folder searchCriteria={folder2} handler={this.onClick}/>
                    <Folder searchCriteria={folder3} handler={this.onClick}/>
                    <Folder searchCriteria={folder4} handler={this.onClickVideos}/>
                </div>
                <div>
                    <div className="content">CONTENT</div>
                    <Search handler={this.onTextChange.bind(this)}/>
                </div>
                <div className="resultBlock">
                {this.state.images.length > 0 ? (
                    <ImageResults  images={this.state.images} />
                ) : null}
                {this.state.videos.length > 0 ? (
                    <VideoResults videos={this.state.videos} />
                ) : null}
                </div>
            </form>
        </div>
        );
    }
}

export default MainApp;