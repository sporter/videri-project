import React, { Component } from 'react';
require("../styles/Folder.css");

class Folder extends Component {
    render() {
        return (
            <div >
                {/*folder is highlighted when selected*/}
                <input id={this.props.searchCriteria} type="radio" name="folder" onClick={this.props.handler}/>
                <label className="folder unselected" htmlFor={this.props.searchCriteria}> </label>
                <div>{this.props.searchCriteria}</div>
            </div>
        );
    }
}

export default Folder;