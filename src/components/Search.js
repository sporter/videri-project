import React, { Component } from 'react';
require('../styles/Searchbox.css');
class Search extends Component {
    render() {
        return (
            <div className="searchBox" >
                <input
                    autoComplete="off"
                    type="text"
                    name="searchText"
                    onChange={this.props.handler}
                    placeholder="Search For Images"
                />
            </div>
        );
    }
}

export default Search;