import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImageItem from './ImageItem';
require("../styles/MainApp.css");
require("../styles/Results.css");

class ImageResults extends Component {
    render() {
        let imageListContent;
        const { images } = this.props;

        // displays overlay and appends image to it on click
        const on = (e, img)=>{
            e.preventDefault();
            document.getElementById("overlay").style.display = "block";
            const image = document.createElement("img");
            image.setAttribute("id", "previewImage");
            image.setAttribute("class", "center");
            image.setAttribute("src", img.largeImageURL);
            document.getElementById("overlay").appendChild(image);
        };

        if (images) {
            imageListContent = (
                <div className={"flex-container"}>
                    {images.map(img => (
                        <ImageItem img={img} handler={(e) => on(e, img)}/>
                    ))}
                </div>
            );
        } else {
            imageListContent = null;
        }

        return (
            <div>
                {imageListContent}
            </div>
        );
    }
}

ImageResults.propTypes = {
    images: PropTypes.array.isRequired
};

export default ImageResults;