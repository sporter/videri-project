import React, { Component } from 'react';
require("../styles/Results.css");

class ImageItem extends Component {
    render() {
        const {img} = this.props;

        const getFileTypeIcon =(image) => {
            const urlParts = image.previewURL.split('/');
            return (urlParts[3] === "photo") ? "/videri-project/build/images/video@2x.png" : "videri-project/build/images/image@2x.png";
        };

        const getResolution =(image) => {
            return image.imageWidth + ' x ' + image.imageHeight;
        };

        const getCreationDate =(image) => {
            const urlParts = image.previewURL.split('/');
            let creationDate = '';
            for (let i = 4; i < 7; i++) {
                creationDate += urlParts[i];
                if(i !== 6) {
                    creationDate += "/";
                }
            }

            return creationDate + ' ' + urlParts[7] + ':' + urlParts[8];
        };

        const getFileName =(image) => { return image.previewURL.substr(image.previewURL.lastIndexOf('/') + 1)};

        const imageTile = {
            margin: '5px',
            background: 'white'
        };

        return (
            <div
                onClick={this.props.handler}
                title={img.tags}
                key={img.id}
                style={imageTile}
            >
                <div className="iconStyle">
                    <img src={img.previewURL} alt='' />
                </div>
                <div className="descriptionStyle">
                    <div>{getFileName(img)}</div>
                    <div><img className="fileTypeIcon" src={window.location.origin + getFileTypeIcon(img)} alt=""/></div>
                    <div>{getResolution(img)}</div>
                    <div>Created</div>
                    <div>{getCreationDate(img)}</div>
                </div>
            </div>
        );
    }
}

export default ImageItem;


