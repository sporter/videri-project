#### A live version can be found at http://seanporteronline.com/ReactImageSearch/build/index.html

### Image search React App
This app utilizes the Pixabay API to search and return images and video results.

### Running The Project Locally
To run this app locally, clone this repo and run these commands in your terminal.
- npm install to install all necessary node_modules
- npm start runs the react-scripts npm script that serves the app locally
```
$ npm install
$ npm start
```
